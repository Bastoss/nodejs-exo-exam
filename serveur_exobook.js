const express = require("express");
const { MongoClient } = require("mongodb");
const app = express();
const port = 3000;

// URL de connexion à MongoDB
const url = "mongodb://localhost:27017";
const dbName = "sample_db";

// Connexion à MongoDB
MongoClient.connect(url, { useUnifiedTopology: true })
  .then((client) => {
    console.log("Connected to MongoDB");
    const db = client.db(dbName);
    app.get("/setup", async function (req, res) {
      // Création de la collection "employés"
      db.createCollection("employes");

      // Insertion de plusieurs documents dans la collection "employes"
      db.collection("employes").insertMany(
        [
          {
            name: "Billy BOB",
            age: 35,
            job: "Entrepreneur",
            salary: 234234,
          },

          {
            name: "DSK",
            age: 32,
            job: "Developeurr",
            salary: 55000,
          },

          {
            name: "Nicolas Cage",
            age: 40,
            job: "Acteur",
            salary: 23472343,
          },
        ],
        function (err, info) {
          // En cas d'erreur, renvoie le statut d'erreur
          res.status(err.status).res.send(info);
        }
      );
    });

    // Route pour la réinitialisation de la collection
    app.get("/reset", function (req, res) {
      db.dropCollection("employes")
        .then(res.status(200).res.send("reset"))
        .catch(res.status(500).res.send(res));
    });

    /**
     * requête MongoDB pour trouver tous les documents dans la collection "employes".
     */
    app.get("/all", async function (req, res) {
      const data = await db.collection("employes").find({});
      res.json(await data.toArray());
    });

    /**
     * requête pour trouver tous les documents où l'âge est supérieur à 33.
     */
    app.get("/allage33", async function (req, res) {
      const data = await db.collection("employes").find({ age: { $gt: 33 } });
      res.json(await data.toArray());
    });

    /**
     * requête pour trier les documents dans la collection "employes" par salaire décroissant.
     */
    app.get("/allsalarydesc", async function (req, res) {
      const data = await db
        .collection("employes")
        .find({})
        .sort({ salary: -1 });
      res.json(await data.toArray());
    });

    /**
     * requête pour sélectionner uniquement le nom et le job de chaque document.
     */
    app.get("/docbynomjob", async function (req, res) {
      const data = await db
        .collection("employes")
        .find({}, { projection: { nom: true, job: true } });
      res.json(await data.toArray());
    });

    /**
     * requête pour compter le nombre d'employes par poste.
     */
    app.get("/countbyjob", async function (req, res) {
      const data = await db.collection("employes").aggregate([
        {
          $group: {
            _id: "$job",
            totalEmployes: { $sum: 1 },
          },
        },
        {
          $project: {
            _id: 0,
            job: "$_id",
            totalEmployes: 1,
          },
        },
      ]);
      res.json(await data.toArray());
    });

    /**
     * requête pour mettre à jour le salaire des développeurs à 80000.
     */
    app.put("/update80000", async function (req, res) {
      const data = await db
        .collection("employes")
        .updateMany({ job: "Sniper" }, { $set: { salary: 80000 } });
      res.json("updated to ");
    });

    // Démarrage du serveur
    app.listen(port, () => {
      console.log(`Server is running on port ${port}`);
    });
  })
  .catch((err) => {
    // En cas d'échec de la connexion à MongoDB, log l'erreur
    console.error("Failed to connect to MongoDB:", err);
    process.exit(1);
  });
